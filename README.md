## REST API request router

### Context
Welcome, you've just become a part of a platform team developing a custom API gateway solution for microservices. The more microservices appear on the map - the more we want to ensure consistency across APIs they expose into the world - the more unified and smooth experience for those developers we strive to create...

### Asignment
- Develop a simplistic request router logic as the core of an API Gateway (API GW) that is capable to receive a request, find a service that it needs to route the request to and forward it. You can assume that service discvery is a given and there is no need to package the logic as a deployable service yet.
- Create an architecture diagram and description that will describe how this peace will be packaged, depoyed and fits into the infrastructure and how everything will work.

### Requirements
- Create a routing logic that provides one root endpoint to reach all sevices deeper in the infrastructure.
- While writing code, structure it well in case other functionality might need to be added (rate limiting, collection of stats, etc.). No need to add more, just routing mechanism.
- Add basic authentication layer for the routable APIs and any management APIs you will create.
- You can choose any technology from this list: Python, Node.js (with Js or Typescript), Java, Kotlin, C# or Go.
- Cover the solution with tests and make it as easy as possible to run and test it.
- Provide the documentation in PDF format and include it in the solution package with the code.
- Include README with clear instructions on how to build and run your solution.
- Provide OpenAPI spec for your API.

### Important things to focus on
- Do not forget to think about good performance on the router since added latency should be absolutely minimum.
- Do not try to spend a lot of time developing a perfect solution, get ready for a discussion about further improvements that will be necessary when scale increases instead.
- When writing architectural documentation you can identify potential bottlenecks and future improvements on the API GW as bonus requirement.
